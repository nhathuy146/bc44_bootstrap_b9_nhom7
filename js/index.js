// Dark theme
document.getElementById("switchButton").onclick = function () {
  // header
  document.getElementById("myHeader").classList.toggle("dark");
  let navLinks = document.getElementsByClassName("nav-link");
  for (let i = 1; i < navLinks.length; i++) {
    navLinks[i].classList.toggle("text-white");
  }

  document.getElementsByClassName("dropdown-menu")[0].classList.toggle("dark");
  let dropdownItem = document.getElementsByClassName("dropdown-item");
  for (let i = 0; i < dropdownItem.length; i++) {
    dropdownItem[i].classList.toggle("text-white");
  }

  // slider
  document.getElementById("mySlider").classList.toggle("dark");

  // About
  document.getElementById("myAbout").classList.toggle("dark");
  document
    .getElementsByClassName("main_title")[0]
    .classList.toggle("text-white");
  document.getElementsByClassName("text")[0].classList.toggle("text-muted");

  let btnRead = document.getElementsByClassName("btn_read");
  for (let i = 0; i < btnRead.length; i++) {
    btnRead[i].classList.toggle("text-white");
  }

  // Pet Service
  document.getElementById("myPetSetVice").classList.toggle("dark2");
  let petSVItem = document.getElementsByClassName("pet-service_item");
  for (let i = 0; i < petSVItem.length; i++) {
    petSVItem[i].classList.toggle("dark");
  }

  let petSVTitle = document.getElementsByClassName("pet-service_title");
  for (let i = 0; i < petSVTitle.length; i++) {
    petSVTitle[i].classList.toggle("text-white");
  }

  let petSVText = document.getElementsByClassName("pet-service_text");
  for (let i = 0; i < petSVText.length; i++) {
    petSVText[i].classList.toggle("text-muted");
  }
};
